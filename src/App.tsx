import React, {Component} from 'react';
import {getDisk, getResources} from "./store/disk/actions";
import {DiskState} from "./store/disk/types";
import {AppState} from "./store/index";
import {connect} from "react-redux";
import {ThunkDispatch} from "redux-thunk";
import H from "history";
import {match} from "react-router-dom";
import {withRouter} from "react-router";
import "./public/css/App.css";
import Header from "./components/Header";
import FileContainer from "./components/FileContainer";

interface AppProps {
  disk: DiskState;
  getDisk: () => void;
  getResources: (path:string) => void;
  history: H.History;
  location: H.Location;
  match: match;
}

class App extends Component<AppProps> {
    unlisten: H.UnregisterCallback = () => {};

    componentDidMount() {
        if (sessionStorage.getItem("token")) {
            this.props.getDisk();
            this.props.getResources(this.props.location.pathname);
        } else {     
            let hash:string = this.props.location.hash;
            if (hash) {
                let token:string = new URLSearchParams('?' + hash.slice(1)).get("access_token") || "";
                sessionStorage.setItem("token", token);
                window.location.replace(window.location.href.split('#')[0])
            } else {
                window.location.replace('https://oauth.yandex.ru/authorize?response_type=token&client_id=c4dfcfd7fbee4a17a53059dfb2889bcd');
            }
        }      
    }

    componentWillMount() {
        this.unlisten = this.props.history.listen((location:H.Location<any>, action:H.Action) => {
            this.props.getResources(location.pathname);
        });
    }
    componentWillUnmount() {
        this.unlisten();
    }

    

    render() {
        if (this.props.disk.error) {
            return (
                <>
                <div>{this.props.disk.error.message}</div>
                </>
            )
        }

        return (
            <>
                <Header />
                <FileContainer 
                    fetching={this.props.disk.fetching}
                    history={this.props.history} 
                    files={this.props.disk.resource_items} 
                />
            </>
        );
    }
}

const mapStateToProps = (state: AppState) => ({
    disk: state.disk
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
    getDisk: async () => {
        dispatch(getDisk());
    },
    getResources: async (path:string) => {
        dispatch(getResources(path));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(App));
