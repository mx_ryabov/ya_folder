import {START_FETCH, IResourceItem, DiskState, DiskActionTypes, StartFetchAction, GET_DISK_SUCCESS, FETCH_ERROR, GET_RESOURCES_SUCCESS} from './types';
import {getRequest} from "../../helpers";
import {AnyAction} from "redux";
import {ThunkAction, ThunkDispatch} from "redux-thunk";


export const getDisk = ():ThunkAction<Promise<void>, {}, {}, AnyAction> => async (dispatch:ThunkDispatch<{}, {}, AnyAction>) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching);
    
    try {
        const res: DiskState = await getRequest("https://cloud-api.yandex.net:443/v1/disk", {});
        const success:DiskActionTypes = {type: GET_DISK_SUCCESS, payload: res};
    
        dispatch(success);
        
    } catch(e) {
        const error:DiskActionTypes = {type: FETCH_ERROR, error: e };
        dispatch(error);
    }    
}


export const getResources = (path:string):ThunkAction<Promise<void>, {}, {}, AnyAction> => {
    return async (dispatch:ThunkDispatch<{}, {}, AnyAction>) => {
        const fetching:StartFetchAction = {type: START_FETCH};
        dispatch(fetching);

        try {
            const res:any = await getRequest("https://cloud-api.yandex.net:443/v1/disk/resources", {path});
            const data: IResourceItem[] = res._embedded.items;
            const success:DiskActionTypes = {type: GET_RESOURCES_SUCCESS, payload: data};
        
            dispatch(success);
        } catch (e) {
            const error:DiskActionTypes = {type: FETCH_ERROR, error: e };
            dispatch(error);
        }
    }
}