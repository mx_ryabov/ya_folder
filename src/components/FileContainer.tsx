import React, { Component } from 'react';
import {IResourceItem} from "../store/disk/types";
import {Link} from "react-router-dom";
import H from "history";
import { spawn } from 'child_process';

interface Props {
    files: IResourceItem[];
    history: H.History;
    fetching: boolean;
}

class FileContainer extends Component<Props> {

    _getTypeIcon(mime_type:string):string {
        mime_type = mime_type.split("/")[1];
        switch (mime_type) {
            case "pdf":
                return "picture_as_pdf";

            case "zip":
                case "x-rar":
                    return "archive";

            case "mpeg":
                return "audiotrack"
            
            case "mp4":
                return "videocam"

            default:
                return "insert_drive_file";
        }
    }

    _renderBreadcrumbs():JSX.Element[] {
        let crumbs:string[] = [];
        let linkCrumbs:JSX.Element[] = [];
        let currentUrlState:string[] = this.props.history.location.pathname.split("/");

        if (this.props.history.location.pathname === "/") {
            return [(<span key={0}>Мои файлы</span>)]
        } else {
            linkCrumbs.push((<Link to="/" key={0}>Мои файлы /</Link>));
        }

        currentUrlState.reduce((acc:string, val:string) => {
            crumbs.push(acc);
            return [acc, val].join("/");
        });

        crumbs.shift();
        //this.props.history.location.pathname.split("/").pop()
        
        

        linkCrumbs = linkCrumbs.concat(crumbs.map((el:string, ind:number) => {
            return (
                <Link to={el} key={ind+1}> {el.split("/").pop()} / </Link>
            )
        }));

        linkCrumbs.push((<span key={currentUrlState.length - 1}> {currentUrlState[currentUrlState.length - 1]}</span>));

        return linkCrumbs;
    }

    _renderItems() {
        let content:JSX.Element[];

        content = this.props.files.map((item, ind) => {
            let file;
            if (item.type === "dir") {
                file = (
                    <Link className="folder" to={item.path.split(":")[1]}>
                        <i className="material-icons">folder</i>
                        <span>{item.name}</span>
                    </Link>
                );
            } else {
                file = (
                    <div className="other">
                        <i className="material-icons">{this._getTypeIcon(item.mime_type)}</i>
                        <span>{item.name}</span>
                    </div>
                );
            }
            return (
                <div key={ind} className="file-container__item col s6 m4 l3 xl2">
                    {file}
                </div>
            );
        });

        if (content.length === 0) {
            return (
                <div className="empty_state">
                    <i className="material-icons">find_in_page</i>
                    <span>Папка пуста!</span>
                    <span>Надеемся, что это не ваш диплом...</span>
                </div>
            )
        }

        return content;
    }

    render() {
        let content:JSX.Element;
        if (this.props.fetching) {
            content = (
                <div className="preloader-wrapper big active">
                    <div className="spinner-layer spinner-yandex-only">
                    <div className="circle-clipper left">
                        <div className="circle"></div>
                    </div><div className="gap-patch">
                        <div className="circle"></div>
                    </div><div className="circle-clipper right">
                        <div className="circle"></div>
                    </div>
                    </div>
                </div>
            )
        } else {
            content = (
                <>
                    <div className="file-container__header">
                        <div className="breadcrumbs">{this._renderBreadcrumbs.bind(this)()}</div>
                    </div>
                    <div className="row folders">
                        {this._renderItems.bind(this)()}
                    </div>
                </>
            )
        }
        return (
            <div className="content">
                <div className="file-container">
                    {content}    
                </div>
            </div>
        );
    }
}

export default FileContainer;
