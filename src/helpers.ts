async function postRequest(url:string, data:any) {
    return fetch(url, {
        method: "POST",
        headers: new Headers({ "Content-Type": "application/json" }),
        body: JSON.stringify(data)
    }).then((res) => {
        if (res.status === 200) {
            return res.json();
        } else {
            throw new Error(`Status code: ${res.status}`)
        }
    });
}

async function getRequest(url:string, params:any) {
    let url_fetch = url + "?";
    Object.keys(params).forEach((key:string) => url_fetch += `${key}=${params[key]}&`);
    url_fetch = url_fetch.slice(0, -1);
    
    return fetch(url_fetch, {
        method: "GET",
        headers: new Headers({
            "Content-Type": "application/json",
            "Authorization": "OAuth " + window.sessionStorage.getItem("token")
        }),
    }).then((res) => {
        if (res.status === 200) {
            return res.json();
        } else {
            throw new Error(`Status code: ${res.status}`)
        }
    });
}


export {postRequest, getRequest};
